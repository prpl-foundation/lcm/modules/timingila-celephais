# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Release v1.0.0 - 2024-01-17

### Changed

- Updated method & event parameter handling to match the updated DM 

## Release v0.1.0

### New

- Initial release
