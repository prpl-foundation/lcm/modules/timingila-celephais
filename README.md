# Timingila-celephais library

[[_TOC_]]

## Introduction

The timingila-celephais adaptor library is responsible for handling commands and signals between Timingila and Celephais, providing a mapping between data model and the packager.

## Building and installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    [Get Docker Engine for appropriate platform]( https://docs.docker.com/engine/install/)

    Make sure you user id is added to the docker group:

    ```bash
    sudo usermod -aG docker $USER
    ```

2. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

3. Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites
- [libcelephais](https://gitlab.com/prpl-foundation/lcm/libraries/libcelephais)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [timingila](https://gitlab.com/prpl-foundation/lcm/applications/timingila)

#### Build and Install

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/modules
    cd ~/workspace/lcm/modules
    git clone https://gitlab.com/prpl-foundation/lcm/modules/timingila-celephais.git
    ```
2. Build it

    ```bash
    cd ~/workspace/lcm/modules/timingila-celephais
    make
   ```

### Installing

#### Using make target install

You can install your own compiled version easily by running the install target.

```bash
cd ~/workspace/lcm/modules/timingila-celephais
sudo make install
```
