/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_common.h>
#include <amxb/amxb.h>

#include <lcm/lcm_assert.h>

#include <timingila/timingila_defines.h>
#include <timingila/timingila_signals.h>

#include <timingila/timingila_module.h>

#include <celephais/celephais_defines.h>
#include "timingila_celephais_priv.h"
#include "timingila_common.h"

#define ME "celephais"

#define TIMINGILA_CELEPHAIS_METHOD "timingilacelephaismethod"
#define TIMINGILA_CELEPHAIS_METHOD_UNKNOWN 0x00
#define TIMINGILA_CELEPHAIS_METHOD_CREATEDU 0x01
#define TIMINGILA_CELEPHAIS_METHOD_UPDATEDU 0x02
#define TIMINGILA_CELEPHAIS_METHOD_UNINSTALLDU 0x03

// DM mngr defines
#define AMXM_SELF "self"

#define UNKNOWN "Unknown"
#define WHEN_STR_EMPTY_RETURN_UNKNOWN(str) (((str) && (str)[0] != '\0') ? (str) : UNKNOWN)

#define PARAMS "parameters"

#define SIZE_ALIAS 64

// shared object bookkeeping
static amxm_shared_object_t * so = NULL;
static amxm_module_t* mod = NULL;

// bus context for Celephais
static amxb_bus_ctx_t* amxb_bus_ctx_celephais = NULL;

// track if we are subscribed to celephais events
static bool celephais_subscribed = false;

// command tracker
static timingila_command_llist_type celephais_command_llist;
static timingila_command_id_type_t celephais_command_id_counter = 0;

static void celephais_notify_handler(const char* const sig_name,
                                     const amxc_var_t* const data,
                                     UNUSED void* const priv);

static inline amxb_bus_ctx_t* celephais_get_amxb_bus_ctx(void) {
    return amxb_bus_ctx_celephais;
}

static int celephais_emit_signal(const char* signal_name, const amxc_var_t* const data) {
    int retval = -1;
    SAH_TRACEZ_INFO(ME, "Emitting signal: %s", signal_name);
    retval = timingila_emit_signal(signal_name, data);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldn't emit signal: %s [%d]", signal_name, retval);
    }
    return retval;
}

#ifndef CELEPHAIS_CMD_LIST
#define CELEPHAIS_CMD_LIST "list"
#endif

// Passes any events from celephais to celephais_notify_handler
static int subscribe_to_celephais_events(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Subscribe to: " CELEPHAIS_DM);

    retval = amxb_subscribe(amxb_bus_ctx_celephais,
                            CELEPHAIS_DM,
                            NULL,
                            celephais_notify_handler,
                            NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot subscribe to " CELEPHAIS_DM " [%d]", retval);
    }

    return retval;
}

// Generic callback to be used to clean up the structures
static void celephais_default_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                      amxb_request_t* req,
                                      int status,
                                      void* priv) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS " default request is done - status = %d", status);
    amxb_close_request(&req);
    if(priv) {
        amxc_var_t* args = (amxc_var_t*) priv;
        amxc_var_delete(&args);
    }
    return;
}

static void celephais_pull_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                               amxb_request_t* req,
                               int status,
                               void* priv) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS " pull request is done - status = %d", status);
    if(status != amxd_status_ok) {
        celephais_emit_signal(EVENT_PACKAGER_INSTALL_DU_FAILED, (const amxc_var_t*) priv);
    }
    celephais_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void celephais_rm_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                        amxb_request_t* req,
                        int status,
                        void* priv) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS " remove request is done - status = %d", status);
    if(status != amxd_status_ok) {
        celephais_emit_signal(EVENT_PACKAGER_UNINSTALL_DU_FAILED, (const amxc_var_t*) priv);
    }
    celephais_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void celephais_bundles_list_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                          amxb_request_t* req,
                                          int status,
                                          void* priv) {
    celephais_default_done_cb(NULL, req, status, priv);
    if(status == amxd_status_ok) {
        celephais_emit_signal(EVENT_TIMINGILA_LATE_INIT_DONE, NULL);
    }
}

static void celephais_gc_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                 UNUSED amxb_request_t* req,
                                 int status,
                                 UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS " gc request is done - status = %d", status);
    return;
}

static int celephais_command(const char* command, amxc_var_t* args, amxb_be_done_cb_fn_t cb, void* priv) {
    int retval = -2;
    amxb_request_t* request = NULL;
    char command_id_str[SIZE_COMMAND_ID_STR];
    unsigned int command_id = 0;
    amxc_var_t* celephais_command_args = NULL;

    if(cb == NULL) {
        SAH_TRACEZ_ERROR(ME, "Callback is NULL");
        return retval;
    }

    command_id = celephais_command_id_counter++;

    if(timingila_command_id_to_hex_str(command_id_str, command_id) < 1) {
        SAH_TRACEZ_ERROR(ME, "command_id_str might be malformed");
    }

    amxc_var_new(&celephais_command_args);
    amxc_var_set_type(celephais_command_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, celephais_command_args, CELEPHAIS_COMMAND, command);
    amxc_var_add_key(cstring_t, celephais_command_args, CELEPHAIS_NOTIF_COMMAND_ID, command_id_str);
    amxc_var_add_key(amxc_htable_t, celephais_command_args, CELEPHAIS_COMMAND_PARAMETERS, amxc_var_constcast(amxc_htable_t, args));

    SAH_TRACEZ_INFO(ME, CELEPHAIS CELEPHAIS_COMMAND ": ");
    if(timingila_command_add_new(&celephais_command_llist, command_id, priv)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add command");
    }

    request = amxb_async_call(celephais_get_amxb_bus_ctx(),
                              CELEPHAIS,
                              CELEPHAIS_CMD_COMMAND,
                              celephais_command_args,
                              cb,
                              priv);
    if(request == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot do " CELEPHAIS ".%s()", command);
        return retval;
    }

    retval = 0;
    return retval;
}


static int celephais_pull(const amxc_var_t* const args) {
    int rc = -1;

    const char* url = NULL;
    const char* username = NULL;
    const char* password = NULL;
    const char* duid = NULL;
    
    uint64_t amxb_deferred_callid;
    
    amxc_var_t* celephais_args = NULL;
    amxc_var_t* priv_args = NULL;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        return rc;
    }

    // decode args
    url = GET_CHAR(args, SM_DM_DU_URL);
    username = GET_CHAR(args, SM_DM_DU_USERNAME);
    password = GET_CHAR(args, SM_DM_DU_PASSWORD);
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    amxb_deferred_callid = GET_UINT64(args, AMXB_DEFERRED_CALLID);

    // translate args to celephais
    amxc_var_new(&celephais_args);
    amxc_var_set_type(celephais_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_PULL_URI, url);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_PULL_DUID, duid);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_PULL_USERNAME, username);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_PULL_PASSWORD, password);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CELEPHAIS_METHOD, TIMINGILA_CELEPHAIS_METHOD_CREATEDU);
    amxc_var_add_key(uint64_t, priv_args, AMXB_DEFERRED_CALLID, amxb_deferred_callid);

    rc = celephais_command(CELEPHAIS_CMD_PULL, celephais_args, celephais_pull_done_cb, (void*) priv_args);
    amxc_var_delete(&celephais_args);

    return rc;
}

static int celephais_update(const amxc_var_t* const args) {
    int rc = -1;

    const char* url = NULL;
    const char* username = NULL;
    const char* password = NULL;
    const char* duid = NULL;
    
    uint64_t amxb_deferred_callid;
    
    amxc_var_t* celephais_args = NULL;
    amxc_var_t* priv_args = NULL;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        return rc;
    }

    // decode args
    url = GET_CHAR(args, SM_DM_DU_URL);
    username = GET_CHAR(args, SM_DM_DU_USERNAME);
    password = GET_CHAR(args, SM_DM_DU_PASSWORD);
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    amxb_deferred_callid = GET_UINT64(args, AMXB_DEFERRED_CALLID);

    amxc_var_new(&celephais_args);
    amxc_var_set_type(celephais_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_UPDATE_URI, url);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_UPDATE_DUID, duid);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_UPDATE_USERNAME, username);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_UPDATE_PASSWORD, password);

    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);

    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CELEPHAIS_METHOD, TIMINGILA_CELEPHAIS_METHOD_UPDATEDU);
    amxc_var_add_key(uint64_t, priv_args, AMXB_DEFERRED_CALLID, amxb_deferred_callid);

    //pull cb is fine for now. may be fine in general
    rc = celephais_command(CELEPHAIS_CMD_UPDATE, celephais_args, celephais_pull_done_cb, (void*) priv_args);
    amxc_var_delete(&celephais_args);
    return rc;
}

static int celephais_remove(const amxc_var_t* const args,
                            uint32_t method) {
    int rc = -1;
    const char* duid = NULL;
    const char* version = NULL;
    amxc_var_t* priv_args = NULL;
    amxc_var_t* celephais_args = NULL;
    uint64_t amxb_deferred_callid;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        return rc;
    }

    // decode args
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    version = GET_CHAR(args, SM_DM_DU_VERSION);
    amxb_deferred_callid = GET_UINT64(args, AMXB_DEFERRED_CALLID);

    // translate args to celephais
    amxc_var_new(&celephais_args);
    amxc_var_set_type(celephais_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_REMOVE_DUID, duid);
    amxc_var_add_key(cstring_t, celephais_args, CELEPHAIS_CMD_REMOVE_VERSION, version);


    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CELEPHAIS_METHOD, method);
    amxc_var_add_key(uint64_t, priv_args, AMXB_DEFERRED_CALLID, amxb_deferred_callid);

    rc = celephais_command(CELEPHAIS_CMD_REMOVE, celephais_args, celephais_rm_cb, (void*) priv_args);
    amxc_var_delete(&celephais_args);

    return rc;
}

static void slot_container_uninstall_du_done(const char* const sig_name,
                                             const amxc_var_t* const data,
                                             void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }
    SAH_TRACEZ_INFO(ME, "priv: %p", priv);

    celephais_remove(data, TIMINGILA_CELEPHAIS_METHOD_UNINSTALLDU);
}

static void slot_container_validate_install_du_done(const char* const sig_name,
                                                    const amxc_var_t* const data,
                                                    UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    celephais_pull(data);
}

static void slot_container_validate_update_du_done(const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    celephais_update(data);
}

static void slot_packager_uninstall_du_done(const char* const sig_name,
                                            UNUSED const amxc_var_t* const data,
                                            UNUSED void* priv) {
    amxc_var_t empty_args;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);

    amxc_var_init(&empty_args);
    amxc_var_set_type(&empty_args, AMXC_VAR_ID_HTABLE);
    celephais_command(CELEPHAIS_CMD_GC, NULL, celephais_gc_done_cb, NULL);
    amxc_var_clean(&empty_args);
}

static int event_dustatechange_from_celephais(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        return retval;
    }

    amxm_ret = timingila_dm_event_execute(TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_event_execute(" TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE ") return code %d",
                    amxm_ret);

    retval = 0;
    return retval;
}

// TODO: replace hardcoded keys
static int add_du_from_celephais(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    const char* executionenvref = NULL;
    const char* duid = NULL;
    const char* uuid = NULL;
    const char* version = NULL;
    const char* uri = NULL;
    const char* vendor = NULL;
    const char* name = NULL;
    const char* description = NULL;
    const char* status = NULL;
    char alias[SIZE_ALIAS] = {'\0'};
    amxc_var_t* du_args = NULL;

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        return retval;
    }

    executionenvref = GET_CHAR(data, SM_DM_DU_EE_REF);
    duid = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_DUID);
    version = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_VERSION);
    uri = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_URI);
    vendor = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_VENDOR);
    name = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_NAME);
    description = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_DESCRIPTION);
    status = GET_CHAR(data, SM_DM_DU_STATUS);
    uuid = GET_CHAR(data, SM_DM_DU_UUID);
    if(snprintf(alias, SIZE_ALIAS, "cpe-%s", duid) < 1) {
        SAH_TRACEZ_ERROR(ME, SM_DM_DU_ALIAS " might be malformed");
    }

    amxc_var_new(&du_args);
    amxc_var_set_type(du_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_URL, uri);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_EE_REF, WHEN_STR_EMPTY_RETURN_UNKNOWN(executionenvref));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_VERSION, WHEN_STR_EMPTY_RETURN_UNKNOWN(version));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_ALIAS, alias);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_NAME, WHEN_STR_EMPTY_RETURN_UNKNOWN(name));
    if(status == NULL) {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
    } else {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_STATUS, status);
    }
    if(uuid) {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_UUID, uuid);
    }

    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_VENDOR, WHEN_STR_EMPTY_RETURN_UNKNOWN(vendor));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_DESCRIPTION, WHEN_STR_EMPTY_RETURN_UNKNOWN(description));

    retval = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, du_args, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_DU ") return code %d",
                    retval);
    amxc_var_delete(&du_args);

    return retval;
}

// Updates a Deployment Unit with data recieved from celephais
static int update_du_from_celephais(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        return retval;
    }

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_DU ") return code %d",
                    amxm_ret);

    retval = 0;

    return retval;
}

// Sends a call to Timingila to remove the Deployment Unit with
// the specified UUID
static int remove_du_from_celephais(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        return retval;
    }

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_REMOVE_DU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_REMOVE_DU ") return code %d",
                    amxm_ret);

    return retval;
}

// Parses Deployment Unit data, and calls update_du_from_celephais
// with only the required Deployment Unit parameters
static int update_du_from_celephais_parsed(amxc_var_t* data) {
    int retval = -1;
    const char* duid = NULL;
    const char* version = NULL;
    const char* url = NULL;
    const char* vendor = NULL;
    const char* description = NULL;
    const char* name = NULL;
    amxc_var_t* args = NULL;

    if(data == NULL) {
        return retval;
    }

    duid = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CELEPHAIS_DM_BUNDLE_DUID);
        return retval;
    }

    version = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_VERSION);
    url = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_URI);
    vendor = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_VENDOR);
    description = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_DESCRIPTION);
    name = GET_CHAR(data, CELEPHAIS_DM_BUNDLE_NAME);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_URL, url);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VENDOR, vendor);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DESCRIPTION, description);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_NAME, name);

    retval = update_du_from_celephais(args);
    amxc_var_delete(&args);

    return retval;
}

// Parses Deployment Unit data, and calls remove_du_from_celephais
// passing the UUID from the Deployment Unit
static int remove_du_from_celephais_parsed(amxc_var_t* notification_parameters) {
    int retval = 1;
    const char* duid = NULL;
    const char* version = NULL;
    amxc_var_t* args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        return retval;  
    }

    duid = GET_CHAR(notification_parameters, CELEPHAIS_DM_BUNDLE_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CELEPHAIS_DM_BUNDLE_DUID);
        return retval;
    }

    version = GET_CHAR(notification_parameters, CELEPHAIS_DM_BUNDLE_VERSION);
    if(version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CELEPHAIS_DM_BUNDLE_VERSION);
        return retval;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);

    retval = remove_du_from_celephais(args);
    amxc_var_delete(&args);

    return retval;
}

static int update_amxb_bus_ctx_celephais(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Update amxb bus ctx for: " CELEPHAIS_DM);

    amxb_bus_ctx_celephais = amxb_be_who_has(CELEPHAIS_DM);
    if(!amxb_bus_ctx_celephais) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CELEPHAIS_DM);
        return retval;
    }

    retval = 0;
    return retval;
}

static void init_command_id_counter(void) {
    SAH_TRACEZ_INFO(ME, "Intialize command id counter with random");
    celephais_command_id_counter = timingila_init_command_id_counter();
    SAH_TRACEZ_INFO(ME, "celephais_command_id_counter = %X", celephais_command_id_counter);
    return;
}

static void init_celephais_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Initialize celephais_command_llist");
    if(timingila_command_llist_init(&celephais_command_llist)) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize celephais_command_llist");
        return;
    }
}

static void slot_celephais_up(const char* const sig_name,
                              UNUSED const amxc_var_t* const data,
                              UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(celephais_subscribed) {
        return;
    }
    if(update_amxb_bus_ctx_celephais() < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not initialize Celephais bus context");
        return;
    }
    subscribe_to_celephais_events();
    celephais_command(CELEPHAIS_CMD_LIST, NULL, celephais_bundles_list_done_cb, NULL);
    celephais_subscribed = true;
}

static int add_slot(const char* const signal_name,
                    const char* const expression,
                    amxp_slot_fn_t fn,
                    void* const priv) {
    int retval = -255;
    if((signal_name == NULL) || (signal_name[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        return retval;
    }

    if(fn == NULL) {
        SAH_TRACEZ_ERROR(ME, "function is NULL");
        return retval;
    }

    SAH_TRACEZ_INFO(ME, "Adding slot: %s [%p]",
                    signal_name,
                    fn);

    retval = timingila_add_slot_to_signal(signal_name, expression, fn, priv);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Error adding slot [%d]", retval);
    }

    return retval;
}

static int remove_slot(const char* const signal_name,
                       amxp_slot_fn_t fn) {
    int retval = -255;
    SAH_TRACEZ_INFO(ME, "Removing slot: %s [%p]", signal_name, fn);
    if((signal_name == NULL) || (signal_name[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        return retval;
    }

    if(fn == NULL) {
        SAH_TRACEZ_ERROR(ME, "function is NULL");
        return retval;
    }

    retval = timingila_remove_slot_from_signal(signal_name, fn);
    return retval;
}

static int celephais_rm_on_uninstall(UNUSED const char* function_name,
                                            amxc_var_t* args,
                                            UNUSED amxc_var_t* ret) {
    int rc = -1;
    bool rm_after_uninstall = false;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        return rc;
    }

    rm_after_uninstall = GET_BOOL(args, "enable");
    if(rm_after_uninstall) {
        SAH_TRACEZ_WARNING(ME, "Enable rmafteruninstall");
        add_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, NULL, slot_packager_uninstall_du_done, NULL);
    } else {
        SAH_TRACEZ_WARNING(ME, "Disable rmafteruninstall");
        remove_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, slot_packager_uninstall_du_done);
    }

    return rc;
}

static void cleanup_celephais_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Cleanup celephais_command_llist");
    timingila_command_llist_cleanup(&celephais_command_llist);
}

static void handle_error_notification(timingila_command_entry_t* cmd, const amxc_var_t* const data) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS_NOTIF_ERROR);
    if(cmd) {
        // Command found so procedure started by us
        // TODO: differentiate between updatedu and installdu ???

        uint64_t amxb_deferred_callid = GET_UINT64(cmd->priv, AMXB_DEFERRED_CALLID);
        const char* uuid = GET_CHAR(cmd->priv, SM_DM_DU_UUID);
        uint32_t faulttype = GET_UINT32(data, CELEPHAIS_NOTIF_ERROR_TYPE);
        const char* faulttypestr = GET_CHAR(data, CELEPHAIS_NOTIF_ERROR_TYPESTR);
        amxc_var_t* notif_data = NULL;

        // cleanup deferred call
        {
            amxc_var_t deferred_data;
            amxc_var_init(&deferred_data);
            amxc_var_set_type(&deferred_data, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(uint32_t, &deferred_data, AMXB_DEFERRED_ERR_CODE, faulttype);
            amxc_var_add_key(cstring_t, &deferred_data, AMXB_DEFERRED_ERR_MSG, faulttypestr);

            amxd_function_deferred_done(amxb_deferred_callid, amxd_status_unknown_error, NULL, &deferred_data);

            amxc_var_clean(&deferred_data);
        }

        amxc_var_new(&notif_data);
        amxc_var_set_type(notif_data, AMXC_VAR_ID_HTABLE);
        
        amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
        amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_EVENT_DUSTATECHANGE_FAILED);
        amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL);
        amxc_var_add_key(uint32_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, faulttype);
        amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, faulttypestr);
        
        event_dustatechange_from_celephais(notif_data);
        amxc_var_delete(&notif_data);
    }
}

static void handle_bundle_pulled_notification(timingila_command_entry_t* cmd, const amxc_var_t* const data) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS_NOTIF_BUNDLE_PULLED);
    amxc_var_t* parameters = GET_ARG(data, PARAMS);
    if(cmd) {
        const char* name = GET_CHAR(parameters, CELEPHAIS_DM_BUNDLE_NAME);
        const char* version = GET_CHAR(parameters, CELEPHAIS_DM_BUNDLE_VERSION);
        const char* vendor = GET_CHAR(parameters, CELEPHAIS_DM_BUNDLE_VENDOR);
        const char* description = GET_CHAR(parameters, CELEPHAIS_DM_BUNDLE_DESCRIPTION);
        const char* duid = GET_CHAR(parameters, CELEPHAIS_DM_BUNDLE_DUID);
        const char* oldversion = GET_CHAR(cmd->priv, SM_DM_DU_VERSION);
        const char* uuid = GET_CHAR(cmd->priv, SM_DM_DU_UUID);
        uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_CELEPHAIS_METHOD);
        const char* executionenvref = GET_CHAR(cmd->priv, SM_DM_DU_EE_REF);

        if(duid) {
            SAH_TRACEZ_INFO(ME, "We have CMD for DUID: [%s]", duid);
        }

        if(executionenvref) {
            amxc_var_add_key(cstring_t, parameters, SM_DM_DU_EE_REF, executionenvref);
        }

        switch(method) {
        case TIMINGILA_CELEPHAIS_METHOD_CREATEDU:
            amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
            amxc_var_add_key(cstring_t, parameters, SM_DM_DU_UUID, uuid);
            add_du_from_celephais(parameters);
            break;
        case TIMINGILA_CELEPHAIS_METHOD_UPDATEDU:
            if(uuid && version) {
                amxc_var_t rmprivargs;
                amxc_var_init(&rmprivargs);
                amxc_var_set_type(&rmprivargs, AMXC_VAR_ID_HTABLE);

                amxc_var_add_key(cstring_t, &rmprivargs, SM_DM_DU_DUID, duid);
                amxc_var_add_key(cstring_t, &rmprivargs, SM_DM_DU_VERSION, oldversion);

                celephais_remove(&rmprivargs, TIMINGILA_CELEPHAIS_METHOD_UPDATEDU);
                amxc_var_clean(&rmprivargs);
            }
            amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UPDATING);
            update_du_from_celephais_parsed(parameters);
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Cannot determine method (%u)", method);
            break;
        }

        if(name && version && vendor && description) {
            // Remove previous version so it can be overriden for new (update case)
            amxc_var_t* varret = amxc_var_take_key(cmd->priv, SM_DM_DU_VERSION);
            if(varret) {
                free(varret);
                varret = NULL;
            }
            amxc_var_add_key(cstring_t, cmd->priv, SM_DM_DU_NAME, name);
            amxc_var_add_key(cstring_t, cmd->priv, SM_DM_DU_VERSION, version);
            amxc_var_add_key(cstring_t, cmd->priv, SM_DM_DU_VENDOR, vendor);
            amxc_var_add_key(cstring_t, cmd->priv, SM_DM_DU_DESCRIPTION, description);

            switch(method) {
            case TIMINGILA_CELEPHAIS_METHOD_CREATEDU:
                celephais_emit_signal(EVENT_PACKAGER_INSTALL_DU_DONE, cmd->priv);
                break;
            case TIMINGILA_CELEPHAIS_METHOD_UPDATEDU:
                celephais_emit_signal(EVENT_PACKAGER_UPDATE_DU_DONE, cmd->priv);
                break;
            default:
                SAH_TRACEZ_ERROR(ME, "Cannot determine method (%u)", method);
            }

        }
    } else {
        // for events not issued by commands of this module
        // not sure if it will work though
        amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
        add_du_from_celephais(parameters);

    }
}

static void handle_bundle_marked_rm_notification(timingila_command_entry_t* cmd, const amxc_var_t* const data) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS_NOTIF_BUNDLE_MARKED_RM);
    if(cmd) {
        uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_CELEPHAIS_METHOD);
        if(method == TIMINGILA_CELEPHAIS_METHOD_UNINSTALLDU) {
            // operation complete
            {
                uint64_t amxb_deferred_callid = GET_UINT64(cmd->priv, AMXB_DEFERRED_CALLID);
                amxd_function_deferred_done(amxb_deferred_callid, amxd_status_ok, NULL, NULL);
            }
            // TODO? The modified status SM_DM_DU_STATUS_UNINSTALLED is not sent to the signal/slot
            celephais_emit_signal(EVENT_PACKAGER_UNINSTALL_DU_DONE, cmd->priv);
        }
    }
    
    amxc_var_t* parameters = GET_ARG(data, PARAMS);
    amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UNINSTALLED);
    update_du_from_celephais_parsed(parameters);
}

static void handle_bundle_removed_notification(const amxc_var_t* const data) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS_NOTIF_BUNDLE_REMOVED);
    amxc_var_t* parameters = GET_ARG(data, PARAMS);

    amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UNINSTALLED);
    amxc_var_add_key(cstring_t, parameters, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UNINSTALL);
    
    event_dustatechange_from_celephais(parameters);
    remove_du_from_celephais_parsed(parameters);
}

static void handle_bundle_list_notification(const amxc_var_t* const data) {
    SAH_TRACEZ_INFO(ME, CELEPHAIS_NOTIF_BUNDLE_LIST);
    amxc_var_t* bundles = GET_ARG(data, CELEPHAIS_NOTIF_BUNDLES);
    if(bundles) {
        amxc_var_for_each(bundle, bundles) {
            bool markforremove = GET_BOOL(bundle, CELEPHAIS_DM_BUNDLE_MARK_RM);
            if(!markforremove) {
                update_du_from_celephais_parsed(bundle);
            }
        }
    }
}

static void celephais_notify_handler(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    timingila_command_entry_t* cmd = NULL;
    SAH_TRACEZ_INFO(ME, AMXM_NOTIFICATION " received [%s]:", sig_name);

    const char* command_id = GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID);
    const char* notification = GET_CHAR(data, AMXM_NOTIFICATION);

    if(notification == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find notification field");
        return;
    }

    if(command_id && (command_id[0] != '\0')) {
        cmd = timingila_command_find(&celephais_command_llist, timingila_command_id_from_hex_str(command_id));
    }

    if(strcmp(notification, CELEPHAIS_NOTIF_ERROR) == 0) {
        handle_error_notification(cmd, data);

    } else if(strcmp(notification, CELEPHAIS_NOTIF_BUNDLE_PULLED) == 0) {
        handle_bundle_pulled_notification(cmd, data);

    } else if(strcmp(notification, CELEPHAIS_NOTIF_BUNDLE_MARKED_RM) == 0) {
        handle_bundle_marked_rm_notification(cmd, data);

    } else if(strcmp(notification, CELEPHAIS_NOTIF_BUNDLE_REMOVED) == 0) {
        handle_bundle_removed_notification(data);

    } else if(strcmp(notification, CELEPHAIS_NOTIF_BUNDLE_LIST) == 0) {
        handle_bundle_list_notification(data);

    } else if(strcmp(notification, AMXB_APP_START) == 0) {
        SAH_TRACEZ_WARNING(ME, CELEPHAIS " is started again, the module will work again");
    } else if(strcmp(notification, AMXB_APP_STOP) == 0) {
        SAH_TRACEZ_ERROR(ME, CELEPHAIS " is stopped, the module will not work properly");
    }

    if(cmd) {
        SAH_TRACEZ_ERROR(ME, "removing command: %s", command_id);
        timingila_command_remove(&cmd);
    }

    return;
}

static void init_slots(void) {
    SAH_TRACEZ_INFO(ME, "Initializing slots");
    add_slot(EVENT_CONTAINER_UNINSTALL_DU_DONE, NULL, slot_container_uninstall_du_done, NULL);
    add_slot(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE, NULL, slot_container_validate_install_du_done, NULL);
    add_slot(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE, NULL, slot_container_validate_update_du_done, NULL);
    add_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, NULL, slot_packager_uninstall_du_done, NULL);
    add_slot("wait:done", NULL, slot_celephais_up, NULL);
    return;
}

static void remove_slots(void) {
    SAH_TRACEZ_INFO(ME, "Removing slots");
    remove_slot(EVENT_CONTAINER_UNINSTALL_DU_DONE, slot_container_uninstall_du_done);
    remove_slot(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE, slot_container_validate_install_du_done);
    remove_slot(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE, slot_container_validate_update_du_done);
    remove_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, slot_packager_uninstall_du_done);
    remove_slot("wait:done", slot_celephais_up);
    return;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-celephais constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(ME, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, MOD_TIMINGILA_PACKAGER)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", MOD_TIMINGILA_PACKAGER);
        goto exit;
    }

    init_celephais_command_llist();
    init_command_id_counter();
    update_amxb_bus_ctx_celephais();
    amxm_module_add_function(mod, TIMINGILA_PACKAGER_RMAFTERUNINSTALL, celephais_rm_on_uninstall);
    init_slots();
    // a "wait:done" signal will be generated when all the objects are present on the bus
    amxb_wait_for_object(CELEPHAIS);

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< timingila-celephais constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-celephais destructor\n");
    remove_slots();
    cleanup_celephais_command_llist();
    amxm_module_deregister(&mod);
    SAH_TRACEZ_INFO(ME, "<<< timingila-celephais destructor\n");
    return 0;
}
